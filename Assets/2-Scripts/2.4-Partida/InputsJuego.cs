using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using TMPro;


public class InputsJuego : MonoBehaviourPunCallbacks
{
    Button btSaltar;
    Button btSalirJuego;
    Joystick stickMovimiento;
    Joystick stickApuntar;

    Jugador jugador;
    [HideInInspector] public Canvas canvas;

    void Awake()
    {
        jugador = GetComponent<Jugador>();
        canvas = FindObjectOfType<Canvas>();
        Button[] botones = canvas.GetComponentsInChildren<Button>();
        Joystick[] sticks = canvas.GetComponentsInChildren<Joystick>();
        btSaltar = botones[0];
        btSalirJuego = botones[1];
        stickMovimiento = sticks[0];
        stickApuntar = sticks[1];
    }

    void Start()
    {
        btSaltar.onClick.AddListener(() =>
        {
            if (jugador.EstaEnElSuelo)
                jugador.Saltar();
        });

        btSalirJuego.onClick.AddListener(() =>
        {
            //StartCoroutine(GameController.gameController.Ganador()); ///provisional paar pruebas, el bueno es el siguiente
            NetworkManager.networkManager.SalirDelNivel();
        });
    }

    void Update()
    {
        
        jugador.Horizontal = stickMovimiento.Horizontal;
        jugador.StickApuntar = new Vector2(stickApuntar.Horizontal,stickApuntar.Vertical) ;

        if(Input.GetAxisRaw("Horizontal")<-0.01f || Input.GetAxisRaw("Horizontal") >0.01f)
            jugador.Horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump") && jugador.EstaEnElSuelo)
        {
            jugador.Saltar();
        }
        if (Input.GetButtonDown("Fire3"))
        {
           // FindObjectOfType<PortalManager>().CreaAgujero();
        }
        if (Input.GetButtonDown("Fire2"))
        {
            jugador.Disparar();
        }

    }
}
