using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviourPunCallbacks
{

    [Header("TextosUI")]
    [SerializeField] TMP_Text txNick;
    [SerializeField] TMP_Text txPuntos;
    [SerializeField] TMP_Text txVida;
    [SerializeField] GameObject jugador;
    [SerializeField] Canvas canvasMuerte;
    [SerializeField] Canvas canvasGanador;
    [SerializeField] TMP_Text puntosConseguidosM;
    [SerializeField] TMP_Text puntosTotalesM;
    [SerializeField] TMP_Text puntosConseguidosG;
    [SerializeField] TMP_Text puntosTotalesG;
    [SerializeField] Button btContinuar;
    [SerializeField] List<GameObject> zonaDeMuerte;
    public static GameController gameController;
    int vida;
    int puntos;
    int bajas;
    Jugador impactado;

    /// <summary>
    /// 
    /// </summary>
    void Awake()
    {
        gameController = this;
        vida = 100;
        puntos = 0;
        bajas = 0;
        
    }

    /// <summary>
    /// 
    /// </summary>
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        photonView.RPC("InstanciarJugador", RpcTarget.All);
        photonView.RPC("InicializarDatosPartidaJugador", RpcTarget.All);

        if (PhotonNetwork.IsMasterClient)
            photonView.RPC("ReproducirMusicaRPC", RpcTarget.AllBufferedViaServer, "AudioM" + UnityEngine.Random.Range(1, 6).ToString() + "Juego");
        if (PhotonNetwork.PlayerList.Length <= 1)
            DestruirZonaDeMuerte();
    }


    [PunRPC]
    public void ReproducirMusicaRPC(string audio)
    {
        AudioManager.audioManager.ReproducirMusica(audio);
    }

    /// <summary>
    /// 
    /// </summary>
    [PunRPC]
    public void InicializarDatosPartidaJugador()
    {
        txPuntos.text = PuntosToString();
        txVida.text = VidaToString();
        txNick.text = GameStatus.usuario.Nick;
    }


    /// <summary>
    /// 
    /// </summary>
    [PunRPC]
    public void InstanciarJugador()
    {
        PhotonNetwork.Instantiate("Jugador", Utiles.PosicionAleatoria(0, 10, -3, -3), Quaternion.identity);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="puntosASumar"></param>
    public void SumarPuntos(int puntosASumar)
    {
        puntos += puntosASumar;
        txPuntos.text = PuntosToString();
 
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="porcentaje"></param>
    /// <param name="impact"></param>
    public void SumarRestarVida(int porcentaje, Jugador impact)
    {
        impactado = impact;
        vida = vida + porcentaje;
        if (vida > 200)
        {
            vida = 200;
        }       
        txVida.text = VidaToString();
        if (vida <= 0)
        {
            StartCoroutine(FinalizarPartida());
        }

    }

    public void DestruirZonaDeMuerte()
    {
        foreach(GameObject go in zonaDeMuerte)
        {
            Destroy(go);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private IEnumerator FinalizarPartida()
    {
        //impactado.GetComponent<Jugador>().photonView.RPC("HaMuerto", RpcTarget.All);
        //impactado.GetComponent<Jugador>().HaMuerto();
        if (PhotonNetwork.CurrentRoom.PlayerCount<=2)
            DestruirZonaDeMuerte();
        impactado.HaMuerto();
        AudioManager.audioManager.ReproducirAudioJuego("AudioMuerte", 5, new Vector3());
        canvasMuerte.gameObject.SetActive(true);
        btContinuar.interactable = false;
        string puntosCons = puntosConseguidosM.text + " ";
        string puntosTot = puntosTotalesM.text + "         ";
        int puntuacionObtenida = puntos;
        yield return new WaitForSeconds(1f);
        puntosConseguidosM.text = puntosCons + puntuacionObtenida;
        yield return new WaitForSeconds(1f);
        puntosTotalesM.text = puntosTot + GameStatus.usuario.PuntosTotales;
        yield return new WaitForSeconds(1f);
        while (puntuacionObtenida > 0)
        {
            puntuacionObtenida--;
            GameStatus.usuario.PuntosTotales++;
            puntosConseguidosM.text = puntosCons + puntuacionObtenida;
            puntosTotalesM.text = puntosTot + GameStatus.usuario.PuntosTotales;
            yield return new WaitForSecondsRealtime(0.01f);
        }
        GameStatus.usuario.PartidasJugadas++;
        GameStatus.usuario.NumeroMuertes++;
        GameStatus.usuario.MaximaPuntuacion = GameStatus.usuario.ComprobarPuntucacionMaxima(puntos);

        DBManager db = FindObjectOfType<DBManager>();
        db.ActualizarPuntuacionesUsuario(GameStatus.usuario);

        yield return new WaitForSeconds(2f);
        NetworkManager.networkManager.SalirDelNivel();

        //btContinuar.interactable = true;
        //if (PhotonNetwork.PlayerList.Length > 2)
        //{
        //    btContinuar.onClick.RemoveAllListeners();
        //    btContinuar.onClick.AddListener(() =>
        //    {
        //        NetworkManager.networkManager.SalirDelNivel();
        //    });
        //}
        //else
        //{
        //    NetworkManager.networkManager.SalirDelNivel();
        //}

        Jugador.jugador.Animator.StopPlayback();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public IEnumerator Ganador()
    {
        int multiplicador = 2;
        AudioManager.audioManager.ReproducirAudioJuego("AudioMuerte", 5, new Vector3());
        canvasGanador.gameObject.SetActive(true);
        string stringPuntosConseguidos = puntosConseguidosG.text + " ";////////
        string stringPuntosTotales = puntosTotalesG.text + "         ";
        int puntuacionObtenida = puntos*multiplicador;
        yield return new WaitForSeconds(1f);
        puntosConseguidosG.text += " "+puntuacionObtenida/multiplicador;
        yield return new WaitForSeconds(1f);
        puntosConseguidosG.text += " x "+multiplicador;
        yield return new WaitForSeconds(1f);
        puntosConseguidosG.text += " = "+ puntuacionObtenida;
        yield return new WaitForSeconds(1f);
        puntosConseguidosG.text = stringPuntosConseguidos + puntuacionObtenida;
        puntosTotalesG.text = stringPuntosTotales + GameStatus.usuario.PuntosTotales;
        yield return new WaitForSeconds(1f);
        while (puntuacionObtenida > 0)
        {
            print("debe contar");
            puntuacionObtenida--;
            GameStatus.usuario.PuntosTotales++;
            puntosConseguidosG.text = stringPuntosConseguidos + puntuacionObtenida;
            puntosTotalesG.text = stringPuntosTotales + GameStatus.usuario.PuntosTotales;
            yield return new WaitForSecondsRealtime(0.03f);
        }
        //puntosConseguidos.text = puntosCons + puntuacionObtenida + " x " + multiplicador;
        GameStatus.usuario.PartidasJugadas++;
        GameStatus.usuario.PartidasGanadas++;
        GameStatus.usuario.MaximaPuntuacion = GameStatus.usuario.ComprobarPuntucacionMaxima(puntos);

        DBManager db = FindObjectOfType<DBManager>();
        db.ActualizarPuntuacionesUsuario(GameStatus.usuario);

        yield return new WaitForSeconds(2f);

        NetworkManager.networkManager.SalirDelNivel();


        //Jugador.jugador.Animator.StopPlayback();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string PuntosToString()
    {        
        return "Puntos: " + puntos.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string VidaToString()
    {
        return "Vida: " + vida.ToString() + "%";
    }

}
