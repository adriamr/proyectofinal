using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public enum eAnimaciones { 
    Estatico, 
    Correr,
    EnElAire, 
    ImpulsoSalto,
    CorrerHaciaAtras,
    HaMuerto
}

public class Jugador : MonoBehaviourPunCallbacks ,IPunObservable
{
    [SerializeField] public  float escala = 0.1f;
    [SerializeField] public float radioSuelo = 0.3f;
    [SerializeField] float velocidadMovimiento;
    [SerializeField] float distanciaCamaraNegativa =-20;
    [SerializeField] float retrasoCamara = 0.4f;
    [SerializeField] float alturaCamara = 5;
    [SerializeField] float fuerzaImpulso;
    [SerializeField] Transform referenciaSueloDelante;
    [SerializeField] Transform referenciaSueloDetras;
    [SerializeField] Transform LimbManoDerecha;
    [SerializeField] Transform limbApuntar;
    [SerializeField] Transform posicionArmadoSinApuntar;
    [SerializeField] Transform posicionHombro;
    [SerializeField] public  GameObject empunyarPistola;
    [SerializeField] public  GameObject manoAuxiliar;
    //[SerializeField] public Player jugadorPhoton;


    bool esResistente;
    bool velocidadMaxActiva;
    bool estaMuerto;
    bool tenemosGanador;
    eAnimaciones eAnimacion;
    Animator animator;
    Rigidbody2D rb;
    Pistola pistola;
    GameController gameController;
    public static Jugador jugador;
    bool puedeFinalizarPartida;


    public Vector2 StickApuntar { get; set; }
    public bool EstaEmpunyandoPistola{ get; set; }
    public bool EstaSiendoAbducido { get; set; }
    public bool OrdenSaltar { get; set; }
    public bool CorrerHaciaAtras { get; set; }
    public bool EstaEnElSuelo { get; set; }
    public float Horizontal { get; set; }
    public float VelocidadMovimiento { get => velocidadMovimiento; set => velocidadMovimiento = value; }
    public Rigidbody2D Rb { get => rb; set => rb = value; }
    public float Escala { get => escala; set => escala = value; }
    public Transform LimbApuntar { get => limbApuntar; set => limbApuntar = value; }
    public eAnimaciones EAnimacion { get => eAnimacion; set => eAnimacion = value; }
    public Animator Animator { get => animator; set => animator = value; }
    public bool EstaMuerto { get => estaMuerto; set => estaMuerto = value; }
    public bool VelocidadMaxActiva { get => velocidadMaxActiva; set => velocidadMaxActiva = value; }
    public GameController GameController { get => gameController; set => gameController = value; }
    public bool EsResistente { get => esResistente; set => esResistente = value; }

    private void Awake()
    {
        EsResistente = false;
        puedeFinalizarPartida = false;
        VelocidadMaxActiva = false;
        tenemosGanador = false;
        jugador = this.gameObject.GetComponent<Jugador>();
        EstaSiendoAbducido = false;
        EstaEmpunyandoPistola = false;
        Animator = GetComponent<Animator>();
        Rb = GetComponent<Rigidbody2D>();
        pistola = empunyarPistola.GetComponentInChildren<Pistola>();
        OrdenSaltar = false;
        //PhotonNetwork.NickName = gamestatus.Usuario.Nick;
        //photonView.name = PhotonNetwork.NickName;
        GameController = FindObjectOfType<GameController>();
        EstaMuerto = false;
    }

    private void Start()
    {
        //PermitirFinalizar(120);
    }

    void Update()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1 && !tenemosGanador /*&& puedeFinalizarPartida*/)
        {
            tenemosGanador = true;
            GameController.gameController.DestruirZonaDeMuerte();

            StartCoroutine(GameController.gameController.Ganador()); //se ha comentado para poder probar el juego con un jugador
            //VelocidadMovimiento = 0;
            //EstaSiendoAbducido = true;
        }
        OrdenSaltar = false;
        if (!EstaSiendoAbducido)                  
            AccionMv();

        GestionarAnimacion();
        Animar();
        Camera.main.transform.position = transform.position + new Vector3(0, alturaCamara, distanciaCamaraNegativa);

    }

    private void LateUpdate()
    {
        if (EstaEmpunyandoPistola)
        {
            empunyarPistola.transform.up = empunyarPistola.transform.position - LimbApuntar.position;
        }
        //Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, transform.position + new Vector3(0, alturaCamara, distanciaCamaraNegativa), retrasoCamara);
    }

    private void FixedUpdate()
    {
       // Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, transform.position + new Vector3(0, alturaCamara, distanciaCamaraNegativa), retrasoCamara);
    }

    IEnumerator PermitirFinalizar(float duracion)
    {
        gameObject.SetActive(false);
        yield return new WaitForSeconds(duracion);
        puedeFinalizarPartida = true;
    }

    private void OnTriggerEnter2D(Collider2D elOtro)
    {
        if (elOtro.tag == "bala")
        {
            if (photonView.IsMine && !esResistente)
            {
                GameController.SumarRestarVida(-10, this);
            }
        }
        if (elOtro.tag == "ZonaDeMuerte")
        {
            if (photonView.IsMine)
            {
                GameController.gameController.SumarRestarVida(-200, this);
            }
        }
        if (elOtro.tag == "Vida")
        {
            //elOtro.GetComponent<Items>().ActivarItem(Utiles.Rdm(15, 40));
            if (photonView.IsMine)
            {
                GameController.gameController.SumarRestarVida(10, this);
            }
        }

    }

    public void SumarPuntos(int puntosASumar, string quienDispara) 
    {
        if(GameStatus.usuario.Nick == quienDispara)
           GameController.SumarPuntos(puntosASumar);
    } 

    private void AccionMv()
    {
        DeterminarSiEstaEnSuelo();
        transform.position += new Vector3(Horizontal * VelocidadMovimiento * Time.deltaTime, 0, 0);
        
        if (EstaEmpunyandoPistola)
        {
            if (StickApuntar.x != 0)
            {
                if (StickApuntar.x + transform.position.x < transform.position.x)
                    transform.localScale = Utiles.Invertir(-Escala, Escala);
                else/* if (StickApuntar.x + transform.position.x > transform.position.x)*/
                    transform.localScale = Utiles.Invertir(Escala, Escala);
            }
            else
                transform.localScale = Utiles.OrientarDireccionJugadorMv(transform.localScale, Horizontal, Escala);

            if (StickApuntar.x != 0 || StickApuntar.y != 0)
            {
                float orgura = 0.6f;
                BrazoDireccionApuntar(new Vector3(posicionHombro.position.x + StickApuntar.x * 10, posicionHombro.position.y + StickApuntar.y * 10, 0));
                if (StickApuntar.x < -orgura || StickApuntar.x > orgura || StickApuntar.y < -orgura || StickApuntar.y> orgura)
                {
                    Disparar();
                    //PView.RPC("Disparar", RpcTarget.All);
                }
                   
            }
            else
                BrazoDireccionApuntar(posicionArmadoSinApuntar.position);
        }
        else
             transform.localScale= Utiles.OrientarDireccionJugadorMv(transform.localScale, Horizontal,Escala);
    }

    private void DeterminarSiEstaEnSuelo()
    {
        if (Physics2D.OverlapCircle(referenciaSueloDelante.position, radioSuelo, 1 << 3) || Physics2D.OverlapCircle(referenciaSueloDetras.position, radioSuelo, 1 << 3))
            EstaEnElSuelo = true;
        else
            EstaEnElSuelo = false;
    }

    [PunRPC]
    public void ActivarPistola(bool valor)
    {
        empunyarPistola.gameObject.SetActive(valor);
        manoAuxiliar.gameObject.SetActive(valor);
        EstaEmpunyandoPistola = valor;
        CorrerHaciaAtras = false;
        if (valor == true) pistola.Municion = 300;
        LimbManoDerecha.gameObject.SetActive(!valor);
        LimbApuntar.gameObject.SetActive(valor);
    }

    private void BrazoDireccionApuntar(Vector3 posicion)
    {
        LimbApuntar.position = posicion;
    }

    public void Disparar()
    {       
        pistola.photonView.RPC("Disparar", RpcTarget.All, LimbApuntar.position, GameStatus.usuario.Nick);
    }

    public void Saltar()
    {
        OrdenSaltar = true;
        Rb.AddForce(Vector2.up * fuerzaImpulso, ForceMode2D.Impulse);
    }

    private void Animar()
    {
        if (EAnimacion == eAnimaciones.HaMuerto)
        {
            //estaMuerto = true;
            print("ha muerto (Animar())");
            Animator.SetBool("HaMuerto", EstaMuerto);
        }
        else
        {
            if (EAnimacion == eAnimaciones.Estatico)
                Animator.SetFloat("Movimiento", 0);
            if (EAnimacion == eAnimaciones.Correr || EAnimacion == eAnimaciones.EnElAire || EAnimacion == eAnimaciones.CorrerHaciaAtras)
                Animator.SetFloat("Movimiento", Mathf.Abs(Horizontal));
            Animator.SetBool("EstaEnElSuelo", EstaEnElSuelo);
            Animator.SetBool("CorrerHaciaAtras", CorrerHaciaAtras);
        }

    }

    private void GestionarAnimacion()
    {
        if (EstaMuerto)
        {
            EAnimacion = eAnimaciones.HaMuerto;
        }
        else
        {
            if (Horizontal == 0 && EstaEnElSuelo)
                EAnimacion = eAnimaciones.Estatico;
            if (Horizontal != 0 && EstaEnElSuelo)
                EAnimacion = eAnimaciones.Correr;
            if (!EstaEnElSuelo)
                EAnimacion = eAnimaciones.EnElAire;
            if (OrdenSaltar && EstaEnElSuelo)
                EAnimacion = eAnimaciones.ImpulsoSalto;

            if (StickApuntar.x > 0 && Horizontal <= 0 && EstaEmpunyandoPistola ||
                StickApuntar.x < 0 && Horizontal >= 0 && EstaEmpunyandoPistola)
            {
                EAnimacion = eAnimaciones.CorrerHaciaAtras;
                CorrerHaciaAtras = true;
            }
            if (StickApuntar.x <= 0 && Horizontal <= 0 && EstaEmpunyandoPistola ||
                StickApuntar.x >= 0 && Horizontal >= 0 && EstaEmpunyandoPistola)
            {
                CorrerHaciaAtras = false;
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
           // stream.SendNext(empunyarPistola);
          //  stream.SendNext(manoAuxiliar);
           // stream.SendNext(EstaEmpunyandoPistola);
            stream.SendNext(EstaMuerto);
        }
        else
        {
          //  empunyarPistola = (GameObject)stream.ReceiveNext();
           // manoAuxiliar = (GameObject)stream.ReceiveNext();
           // EstaEmpunyandoPistola = (bool)stream.ReceiveNext();
            EstaMuerto = (bool)stream.ReceiveNext();
        }
    }

    [PunRPC]
    public void HaMuerto()
    {
        EstaMuerto = true;
        GetComponent<Collider2D>().enabled = false;
        GetComponentInChildren<Collider2D>().enabled = false;
        GetComponent<InputsJuego>().canvas.gameObject.SetActive(false);
        VelocidadMovimiento = 0;
    }

    //private void AccionPC()
    //{
    //    if (!EstaSiendoAbducido)
    //    {
    //        bool saltar = false;
    //        Horizontal = Input.GetAxisRaw("Horizontal");
    //        transform.position += new Vector3(Horizontal * velocidadMovimiento * Time.deltaTime, 0, 0);
    //        //rb.velocity = new Vector2(Horizontal*velocidadMovimiento*Time.deltaTime, rb.velocity.y);
    //        EstaEnElSuelo = Physics2D.OverlapCircle(referenciaSuelo.position, 1f, 1 << 3);

    //        if (Input.GetButtonDown("Jump") && EstaEnElSuelo)
    //        {
    //            saltar = true;
    //            rb.AddForce(Vector2.up * fuerzaImpulso, ForceMode2D.Impulse);
    //            // RaycastHit2D rayo = Physics2D.Raycast(transform.position, new Vector2(0, -1));
    //            // if (rayo.collider != null) 
    //        }

    //        if (EstaEmpunyandoPistola)
    //        {
    //            if (Input.GetButtonDown("Fire1"))
    //            {
    //                Disparar();
    //            }
    //            if (LimbManoDerecha.transform.position.x < transform.position.x)
    //                transform.localScale = Utiles.Invertir(-escala,escala);
    //            if (LimbManoDerecha.transform.position.x > transform.position.x)
    //                transform.localScale = Utiles.Invertir(escala, escala);
    //            LimbManoDerecha.position =
    //                Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));

    //        }
    //        else
    //        {
    //            transform.localScale = Utiles.OrientarDireccionJugadorMv(transform.localScale, Horizontal, escala);
    //        }
    //    }
    //    else
    //    {
    //        transform.localScale = Utiles.OrientarDireccionJugadorMv(transform.localScale, Horizontal, escala);
    //    }
    //}
}
