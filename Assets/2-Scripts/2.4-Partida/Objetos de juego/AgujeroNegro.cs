using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class AgujeroNegro : MonoBehaviourPunCallbacks
{
    [SerializeField] GameObject agujeroBlanco;
    [SerializeField] Transform posicionAgujeroBlanco;
    Jugador jugador;

    public Jugador Jugador { get => jugador; set => jugador = value; }

    /// <summary>
    /// M�todo que se ejecuta al iniciar el objeto, reproduce el audio de abrir agujero
    /// </summary>
    void Start()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioAbrirAgujero", 2f, transform.position);
    }

    /// <summary>
    /// Trigger que se ejecuta cuando colisiona este Collider2D con otro, si colisiona con un jugador
    /// </summary>
    /// <param name="elOtro">Collider2D con el que ha colisionado con este Collider2D</param>
    private void OnTriggerEnter2D(Collider2D elOtro)
    {
        if (elOtro.CompareTag("Player"))
        {
            ////portalManager = FindObjectOfType<PortalManager>();
            //jugador = elOtro.gameObject.GetComponent<Jugador>();
            //jugador.transform.position = transform.position;
            //PrepararParaAbducir();
            //CrearAgujeroBlanco();
            //StartCoroutine(AbducirJugador());

            Jugador = elOtro.gameObject.GetComponent<Jugador>();
            Jugador.transform.position = transform.position;
            CrearAgujeroBlanco();         
            //Jugador.transform.position = agujeroBlanco.transform.position;
            StartCoroutine(DesaparecerPorUnMomento());
        }
    }
    IEnumerator DesaparecerPorUnMomento()
    {
        Jugador.EstaSiendoAbducido = true;
        //AudioManager.audioManager.ReproducirAudioJuego("AudioAbducidoPortalNegro", 2f, transform.position);
        Jugador.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        yield return new WaitForSecondsRealtime(0.3f);
        Jugador.transform.position = agujeroBlanco.transform.position;
        Jugador.transform.localScale = new Vector3(Jugador.Escala, Jugador.Escala, Jugador.Escala);
        Jugador.EstaSiendoAbducido = false;

    }

    /// <summary>
    /// M�todo que crea un agujero blanco en la posici�n de posicionAgujeroBlanco
    /// </summary>
    private void CrearAgujeroBlanco()
    {
        agujeroBlanco = PhotonNetwork.Instantiate("AgujeroBlanco", posicionAgujeroBlanco.position, Quaternion.identity);
    }

    /// <summary>
    /// M�todo que prepara algunos valores para la correcta abducci�n del jugador.
    /// </summary>
    private void PrepararParaAbducir()
    {
        Jugador.Rb.gravityScale = 0;
        Jugador.Rb.bodyType = RigidbodyType2D.Static;
        //jugador.EAnimacion = eAnimaciones.Estatico;
        Jugador.transform.position = transform.position;
        Jugador.EstaSiendoAbducido = true;
    }

    /// <summary>
    /// Corrutina que va reduciendo el tama�o del jugador al tiempo lo rota sobre si mismo de manera acelerada.
    /// Invoca la corrutina Reaparecer(Jugador) de AgujeroBlanco
    /// </summary>
    /// <returns></returns>
    IEnumerator AbducirJugador()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioAbducidoPortalNegro", 2f, transform.position);
        float aceleracion = 0.05f;
        float x;
        for (double i = 0.1f; i > 0.03f; i -= 0.001f)
        {
            x = (float)i;
            jugador.gameObject.transform.Rotate(new Vector3(0, 0, aceleracion));
            if (Jugador.Horizontal < 0)
                x = (float)-i;
            Jugador.transform.localScale = new Vector3(x, (float)i, Jugador.Escala);
            aceleracion *= 1.2f;
            yield return new WaitForSecondsRealtime(0.01f);
            print("finalizando corrutina an");
        }
         StartCoroutine(agujeroBlanco.GetComponent<AgujeroBlanco>().Reaparecer(Jugador));
    }

    //IEnumerator Reaparecer()
    //{
    //    AudioManager.audioManager.ReproducirAudioJuego("AudioReaparecerEnAgujeroBlanco", 2f, agujeroBlanco.transform.position);
    //    Jugador.transform.position = agujeroBlanco.transform.position;
    //    float aceleracion = 4969098;
    //    float x = 1;
    //    for (double i = 0.01; i < 0.1f; i += 0.01f)
    //    {
    //        x = (float)i;
    //        jugador.transform.Rotate(new Vector3(0, 0, aceleracion));
    //        if (Jugador.Horizontal < 0)
    //            x = (float)-i;
    //        Jugador.transform.localScale = new Vector3(x, (float)i, Jugador.Escala);
    //        aceleracion /= 1.19f;
    //        print(x);
    //        yield return new WaitForSecondsRealtime(0.1f);
    //        print("finalizando corrutina ab");
    //    }
    //    x = 0.1f;
    //    if (Jugador.Horizontal < 0)
    //        x = -0.1f;

    //    Restablecer(x);

    //}

    //public void Restablecer(float x)
    //{
    //    print("restablecer ");
    //    Jugador.transform.localScale = new Vector3(x, Jugador.Escala, Jugador.Escala);
    //    Jugador.transform.rotation = new Quaternion(0, 0, 0, 0);
    //    Jugador.Rb.gravityScale = 1;
    //    Jugador.Rb.bodyType = RigidbodyType2D.Dynamic;
    //    Jugador.EstaSiendoAbducido = false;
    //}















    /*
     [SerializeField] GameObject agujeroBlanco;
    PhotonView pv;
    PortalManager portalManager;
    public Jugador jugador;
    private void Awake()
    {
        pv = GetComponent<PhotonView>();
    }

    void Start()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioAbrirAgujero", 2f, transform.position);
    }

    private void OnTriggerEnter2D(Collider2D elOtro)
    {
        if (elOtro.CompareTag("Player"))
        {
            //portalManager = FindObjectOfType<PortalManager>();
            ////GameObject jugador = elOtro.gameObject;
            //jugador = elOtro.gameObject.GetComponent<Jugador>();
            //jugador.transform.position = transform.position;
            //PrepararParaAbducir();
            ////pv.RPC("CrearAgujeroBlanco",RpcTarget.All);
            //CrearAgujeroBlanco();
            //Destroy(gameObject, 2f);
            //StartCoroutine(AbducirJugador());

            CrearAgujeroBlanco();
            //pv.RPC("CrearAgujeroBlanco", RpcTarget.All);
            jugador = elOtro.gameObject.GetComponent<Jugador>();
            jugador.transform.position = agujeroBlanco.transform.position;
            Destroy(gameObject, 0.2f);
        }
    }
    //[PunRPC]
    public void CrearAgujeroBlanco()
    {
       // Vector3 posicion= Utiles.PosicionAleatoria(5, 15, -1, -1);
        //Instantiate(agujeroBlanco, posicion, Quaternion.identity);
       // agujeroBlanco.transform.position = posicion;
        agujeroBlanco= PhotonNetwork.Instantiate("AgujeroBlanco", Utiles.PosicionAleatoria(5, 15, -1, -1), Quaternion.identity);
    }

    public void PrepararParaAbducir()
    {
        jugador.Rb.gravityScale = 0;
        jugador.Rb.bodyType = RigidbodyType2D.Static;
        //jugador.EAnimacion = eAnimaciones.Estatico;
        jugador.transform.position = transform.position;
        jugador.EstaSiendoAbducido = true;
    }

    IEnumerator AbducirJugador()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioAbducidoPortalNegro", 2f, transform.position);
        float aceleracion = 0.05f;
        float x;
        for (double i = 0.1f; i > 0.03f; i -= 0.001f)
        {
            x = (float)i;
            //jugador.gameObject.transform.Rotate(new Vector3(0, 0, aceleracion));
            if (jugador.Horizontal < 0)
                x = (float)-i;
            jugador.transform.localScale = new Vector3(x, (float)i, jugador.Escala);
            aceleracion *= 1.2f;
            yield return new WaitForSecondsRealtime(0.01f);
            print("finalizando corrutina an");
        }
        StartCoroutine(Reaparecer());
    }

    IEnumerator Reaparecer()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioReaparecerEnAgujeroBlanco", 2f, agujeroBlanco.transform.position);
        jugador.transform.position = agujeroBlanco.transform.position;
        float aceleracion = 4969098;
        float x = 1;
        for (double i = 0.01; i < 0.1f; i += 0.01f)
        {
            x = (float)i;
            //jugador.transform.Rotate(new Vector3(0, 0, aceleracion));
            if (jugador.Horizontal < 0)
                x = (float)-i;
            jugador.transform.localScale = new Vector3(x, (float)i, jugador.Escala);
            aceleracion /= 1.19f;
            print(x);
            yield return new WaitForSecondsRealtime(0.1f);
            print("finalizando corrutina ab");
        }
        x = 0.1f;
        if (jugador.Horizontal < 0)
            x = -0.1f;

        Restablecer(x);

    }

    public void Restablecer(float x)
    {
        print("restablecer ");
        jugador.transform.localScale = new Vector3(x, jugador.Escala, jugador.Escala);
        jugador.transform.rotation = new Quaternion(0, 0, 0, 0);
        jugador.Rb.gravityScale = 1;
        jugador.Rb.bodyType = RigidbodyType2D.Dynamic;
        jugador.EstaSiendoAbducido = false;
    }*/
}
