using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformasMoviles : MonoBehaviour
{
    private byte siguientePosicion;
    [SerializeField] Transform[] posiciones;
    [SerializeField] float velocidad;
    [SerializeField] float distanciaCambio;

    void Start()
    {
        siguientePosicion = 0;
    }

    void Update()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, posiciones[siguientePosicion].transform.position, velocidad*Time.deltaTime);
        if((Vector3.Distance(this.transform.position, posiciones[siguientePosicion].transform.position)) < distanciaCambio)
        {
            siguientePosicion++;
            if (siguientePosicion >= posiciones.Length)
            {
                siguientePosicion = 0;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D elOtro)
    {
        if (elOtro.gameObject.tag == "Player")
        {
            elOtro.gameObject.transform.position = Vector3.MoveTowards(elOtro.gameObject.transform.position,
                posiciones[siguientePosicion].transform.position, velocidad * Time.deltaTime);
        }
    }
}
