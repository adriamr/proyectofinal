using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class Pistola : MonoBehaviourPunCallbacks
{
    [SerializeField] GameObject bala;
    [SerializeField] Transform salidaBala;
    [SerializeField] float cadencia;
    [SerializeField] int municion;
    bool puedeDisparar;
    public static Vector3 direccionApuntar;

    public int Municion { get => municion; set => municion = value; }


    /// <summary>
    /// M�todo que se reproduce antes de iniciar el game object, inicializa la variable puedeDisparar y la clase audio.
    /// </summary>
    private void Awake()
    {
        puedeDisparar = true;
    }

    /// <summary>
    /// M�todo que instancia una bala en una posici�n, rotaci�n y direcci�n indicada, resta una unidad de munici�n, 
    /// invoca la corrutina EsperaTrasDisparo , y si no hay munici�n invoca la corrutina SinMunicion.
    /// </summary>
    /// <param name="direccion">direcci�n en la que debe ir la bala en relaci�n al personaje</param>
    [PunRPC]
    public void Disparar(Vector3 direccion, string quienDispara)
    {
        //direccionApuntar = (direccion - salidaBala.position).normalized;
        if (puedeDisparar && Municion>0)
        {
            var balaActual = GameObject.Instantiate(bala).GetComponent<BalaPistola>();
            balaActual.transform.position = salidaBala.position;
            balaActual.SetDireccion((direccion - salidaBala.position).normalized);
            balaActual.QuienDispara = quienDispara;

            AudioManager.audioManager.ReproducirAudioJuego("AudioDisparoPistola", 2f, salidaBala.position);
 
            Municion--;
            if (Municion <= 0)
                StartCoroutine(SinMunicion());
            StartCoroutine(EsperaTrasDisparo());
        }
    }

    [PunRPC]
    private void ReproducirAudioArmaRPC(string audio)
    {
        AudioManager.audioManager.ReproducirAudioJuego(audio, 1, transform.position);
    }

    /// <summary>
    /// Corrutina, no permite disparar mientras no transcurra el tiempo de cadencia de la pistola
    /// </summary>
    IEnumerator EsperaTrasDisparo()
    {
        puedeDisparar = false;
        yield return new WaitForSecondsRealtime(cadencia);
        puedeDisparar = true;
    }


    /// <summary>
    /// Corrutina, reproduce tres sonidos de disparo sin munici�n, posteriormente invoca el metodo "ActivarPistola" de la clase Jugador para desactivar la pistola
    /// </summary>
    IEnumerator SinMunicion()
    {
        for (int i=0;i<3; i++)
        {
            //AudioManager.audioManager.ReproducirAudioJuego("AudioSinMunicion", 0.3f, salidaBala.position);
            photonView.RPC("ReproducirAudioArmaRPC", RpcTarget.All, "AudioSinMunicion");
            yield return new WaitForSecondsRealtime(0.3f);
        }
        GetComponentInParent<Jugador>().photonView.RPC("ActivarPistola", RpcTarget.All,false) ;
    }
}
