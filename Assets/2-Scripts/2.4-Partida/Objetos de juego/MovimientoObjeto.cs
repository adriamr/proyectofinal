using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoObjeto : MonoBehaviour
{
    [SerializeField] float velocidad = 0.001f;
    [SerializeField] float ejeX = 0.1f;
    [SerializeField] float ejeY = 1;
    [SerializeField] float ejeZ = 0;
    [SerializeField] float velocidadRotacion = 0.01f;

    void Update()
    {
        Vector3 movimiento = new Vector3(ejeX, ejeY, ejeZ);
        Vector3 rotacion = new Vector3(0, 0, velocidadRotacion);
        transform.Translate(movimiento-rotacion * velocidad * Time.deltaTime);
        transform.Rotate(rotacion * Time.deltaTime) ;
    }
}
