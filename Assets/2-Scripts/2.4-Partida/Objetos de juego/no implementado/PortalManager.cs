using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class PortalManager : MonoBehaviourPunCallbacks
{
    [SerializeField] Transform posicionAgujeroNegro;
    [SerializeField] Transform posicionAgujeroBlanco;
    [SerializeField] GameObject agujeroNegro;
    [SerializeField] GameObject agujeroBlanco;


    private void Awake()
    {
    }
    //[PunRPC]
    void CrearAgujeroNegro()
    {
        //PhotonNetwork.Instantiate("AgujeroNegro", posicionAgujeroNegro.position, Quaternion.identity);
        Instantiate(agujeroNegro, posicionAgujeroNegro.localPosition, Quaternion.identity);
        //agujeroNegro.transform.position = posicion;
    }

    //[PunRPC]
    public void CrearAgujeroBlanco()
    {
        //Instantiate(agujeroBlanco, posicion, Quaternion.identity);
        // agujeroBlanco.transform.position = posicion;
        //agujeroBlanco = PhotonNetwork.Instantiate("AgujeroBlanco", posicionAgujeroBlanco.position, Quaternion.identity);
        Instantiate(agujeroBlanco, posicionAgujeroBlanco.position, Quaternion.identity);
    }

}
