using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class AgujeroBlanco : MonoBehaviourPunCallbacks
{

    /// <summary>
    /// Metodo que se ejecuta al iniciar el objeto, Reproduce el audio abrir agujero en la misma posici�n del mismo, y se autodestruye pasados 5 segundos
    /// </summary>
    void Start()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioAbrirAgujero", 2f, transform.position);
        Destroy(gameObject, 3f);
    }

    /// <summary>
    /// Corrutina que hace reaparecer al jugador agrandandolo desde 0 hasta su tama�o original y rotandolo de forma deselerada a medida que se hace m�s grande
    /// invoca el m�todo Restablecer(float, Jugador)
    /// </summary>
    /// <param name="jugador">El jugador que debe reaparecer</param>
    /// <returns></returns>
    public IEnumerator Reaparecer(Jugador jugador)
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioReaparecerEnAgujeroBlanco", 2f, transform.position);
        jugador.transform.position = transform.position;
        float aceleracion = 4969098;
        float x = 1;
        for (double i = 0.01; i < 0.1f; i += 0.01f)
        {
            x = (float)i;
            jugador.transform.Rotate(new Vector3(0, 0, aceleracion));
            if (jugador.Horizontal < 0)
                x = (float)-i;
            jugador.transform.localScale = new Vector3(x, (float)i, jugador.Escala);
            aceleracion /= 1.19f;
            print(x);
            yield return new WaitForSecondsRealtime(0.1f);
            print("finalizando corrutina ab");
        }
        x = 0.1f;
        if (jugador.Horizontal < 0)
        {
            x = -0.1f;
        }
        Restablecer(x, jugador);
    }

    /// <summary>
    /// M�todo que restablece los valores a como estaban antes del teletransporte del jugador
    /// </summary>
    /// <param name="x">Entero para determinar la orientaci�n del jugador (izquierda o derecha)</param>
    /// <param name="jugador">El jugador que hay que restablecer</param>
    private void Restablecer(float x, Jugador jugador)
    {
        print("restablecer ");
        jugador.transform.localScale = new Vector3(x, jugador.Escala, jugador.Escala);
        jugador.transform.rotation = new Quaternion(0, 0, 0, 0);
        jugador.Rb.gravityScale = 1;
        jugador.Rb.bodyType = RigidbodyType2D.Dynamic;
        jugador.EstaSiendoAbducido = false;
    }
}
