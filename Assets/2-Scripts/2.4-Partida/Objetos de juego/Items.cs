//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Items : MonoBehaviourPunCallbacks
{
    private void Awake()
    {
       // ActivarItem(0, false);
    }

    private void Start()
    {
        //photonView.RPC("DesactivarItemRPC", RpcTarget.All);
        DesactivarItemRPC();
        //ActivarItemRPC();
        StartCoroutine(ActivarItem(Utiles.Rdm(5, 20)));
    }

    IEnumerator ActivarItem(float duracion)
    {
        print("activarIem Corrutina");
        yield return new WaitForSeconds(duracion);
        photonView.RPC("ActivarItemRPC", RpcTarget.All);
        //ActivarItemRPC();
    }

    public void DesactivarItemRPC()
    {
        print("desactivarIem ");
        //gameObject.SetActive(false);
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.GetComponent<CircleCollider2D>().enabled = false;
    }

    [PunRPC]
    public void ActivarItemRPC()
    {
        print("activarIem ");
        //gameObject.SetActive(true);
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        gameObject.GetComponent<CircleCollider2D>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D elOtro)
    {
        if (elOtro.tag == "Player")
        {

            if (this.tag == eTags.Vida.ToString())
            {
                Vida(elOtro);
            }
            if (this.tag == eTags.Velocidad.ToString())
            {
                Velocidad(elOtro);
            }
            if (this.tag == eTags.SuperSalto.ToString())
            {
                SuperSalto(elOtro);
            }
            if (this.tag == eTags.Resistencia.ToString())
            {
                Resistencia(elOtro);
            }
            if (this.tag == eTags.Jetpack.ToString())
            {
                Jetpack(elOtro);
            }
            if (this.tag == eTags.Pistola.ToString())
            {
                Pistola(elOtro);
            }
        }
    }

    private void Jetpack(Collider2D elOtro)
    {
        //implementar jetpack, sonido, y si da tiempo alg�n efecto (opcional)

        //AudioManager.audioManager.ReproducirAudioJuego("AudioJetpack", 2f, transform.position);
        photonView.RPC("ReproducirAudioItemsRPC", RpcTarget.All, "AudioJetpack");

        Destroy(this.gameObject);
    }

    private void Resistencia(Collider2D elOtro)
    {
        Jugador jugador = elOtro.gameObject.GetComponent<Jugador>();
        if (!jugador.EsResistente)
        {
            jugador.EsResistente = true;
            //gameObject.GetComponent<SpriteRenderer>().enabled = false;
            //gameObject.GetComponent<CircleCollider2D>().enabled = false;
            photonView.RPC("ReproducirAudioItemsRPC", RpcTarget.All, "AudioResistencia");

            //photonView.RPC("DesactivarItems", RpcTarget.All, true);
            //ActivarItem(Utiles.Rdm(15, 30), true);
            //photonView.RPC("DesactivarItemRPC", RpcTarget.All);
            DesactivarItemRPC();
            StartCoroutine(ActivarItem(Utiles.Rdm(15, 30)));

            StartCoroutine(ResistenciaImpactos(jugador));
        }
    }

    IEnumerator ResistenciaImpactos(Jugador jugador)
    {
        yield return new WaitForSeconds(10f);
        jugador.EsResistente = false;
    }

    private void SuperSalto(Collider2D elOtro)
    {
        //AudioManager.audioManager.ReproducirAudioJuego("AudioSuperSalto", 2f, transform.position);
        photonView.RPC("ReproducirAudioItemsRPC", RpcTarget.All, "AudioSuperSalto");

    }

    private void Vida(Collider2D elOtro)
    {
        photonView.RPC("ReproducirAudioItemsRPC", RpcTarget.All, "AudioVida");

        //photonView.RPC("DesactivarItems", RpcTarget.All, true);
        //ActivarItem(Utiles.Rdm(15, 40), true);

        //photonView.RPC("DesactivarItemRPC", RpcTarget.All);
        DesactivarItemRPC();
        StartCoroutine(ActivarItem(Utiles.Rdm(30, 40)));
    }

    private void Pistola(Collider2D elOtro)
    {
        //AudioManager.audioManager.ReproducirAudioJuego("AudioCogerArma", 2f,transform.position);
        photonView.RPC("ReproducirAudioItemsRPC", RpcTarget.All, "AudioCogerArma");
        elOtro.gameObject.GetComponent<Jugador>().photonView.RPC("ActivarPistola",RpcTarget.All, true);

        //photonView.RPC("DesactivarItemRPC", RpcTarget.All);
        DesactivarItemRPC();
        StartCoroutine(ActivarItem(Utiles.Rdm(20,40)));
    }


    private void Velocidad(Collider2D elOtro)
    {
        Jugador jugador = elOtro.gameObject.GetComponent<Jugador>();
        if (!jugador.VelocidadMaxActiva)
        {
            //AudioManager.audioManager.ReproducirAudioJuego("AudioVelocidad", 2f, transform.position);
            photonView.RPC("ReproducirAudioItemsRPC", RpcTarget.All, "AudioVelocidad");

            StartCoroutine(VelocidadExtra(jugador));
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<CircleCollider2D>().enabled = false;
        }
    }
    IEnumerator VelocidadExtra(Jugador jugador)
    {
        jugador.VelocidadMovimiento *= 1.4f;
        jugador.VelocidadMaxActiva = true;
        yield return new WaitForSeconds(15f);
        jugador.VelocidadMovimiento /= 1.4f;
        jugador.VelocidadMaxActiva = false;
        //AudioManager.audioManager.ReproducirAudioJuego("AudioVelocidad", 2f, transform.position);
        photonView.RPC("ReproducirAudioItemsRPC", RpcTarget.All, "AudioVelocidad");
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        gameObject.GetComponent<CircleCollider2D>().enabled = true;

        //photonView.RPC("DesactivarItemRPC", RpcTarget.All);
        DesactivarItemRPC();
        StartCoroutine(ActivarItem(Utiles.Rdm(5,10)) );
    }

    [PunRPC]
    public void ReproducirAudioItemsRPC(string audio)
    {
        AudioManager.audioManager.ReproducirAudioJuego(audio, 2, transform.position);
    }

}
