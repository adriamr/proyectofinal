using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaPistola : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField] float velocidad;
    Vector3 direccion;
    private string quienDispara;

    public string QuienDispara { get => quienDispara; set => quienDispara = value; }

    void Start()
    {
        Destroy(this.gameObject, 2.5f);
    }

    void LateUpdate()
    {
        print(direccion);
        print(velocidad);
        transform.position += direccion * velocidad * Time.deltaTime;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //probando
        if (stream.IsWriting)
        {
            stream.SendNext(direccion);
        }
        else
        {
            direccion = (Vector3)stream.ReceiveNext();
        }
    }

    public void SetDireccion(Vector3 dir)
    {
        direccion = dir;
    }

    /// <summary>
    /// Evento, detecta si el objecto colisionado tiene el tag "Player" y si es as� suma puntos al jugador atacante y luego se autodestruye la bala
    /// </summary>
    /// <param name="elOtro">Collider2D del objeto impactado</param>
    private void OnTriggerEnter2D(Collider2D elOtro)
    {
        string elOtroTag = elOtro.gameObject.tag;
        if (elOtroTag == "Player")
        {
           // if (quienDirparo == PhotonNetwork.NickName)
            Jugador.jugador.SumarPuntos(10, QuienDispara);
            AudioManager.audioManager.ReproducirAudioJuego("AudioImpactoBala"+Random.Range(1,4).ToString(),1,transform.position);
            //photonView.RPC("ReproducirAudioBalaRPC", RpcTarget.All, "AudioImpactoBala" + Random.Range(1, 4).ToString()+"");
            Destroy(gameObject, 0.01f);
        }
        else
        {
            //buscar un sonido de impacto con alguna superficie
            // AudioManager.audioManager.ReproducirAudio("", 1f, transform.position);
            if (elOtroTag != "Resistencia" && elOtroTag != "Velocidad" && elOtroTag != "Vida" && elOtroTag != "Pistola")
                Destroy(gameObject, 0.01f);
        }
        //if(elOtroTag!= "Resistencia" && elOtroTag!="Velocidad" && elOtroTag!="Vida" && elOtroTag!="Pistola")
        //    Destroy(gameObject,0.01f);
    }

    [PunRPC]
    public void ReproducirAudioBalaRPC(string audio)
    {
        AudioManager.audioManager.ReproducirAudioJuego(audio,2, transform.position);
    }
}
