using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetaEnOrbita : MonoBehaviour
{
    [SerializeField] float velocidad;
    [SerializeField] float ejeX;
    [SerializeField] float ejeY;
    [SerializeField] float ejeZ;
    [SerializeField] float angulo;
    [SerializeField] GameObject puntoDeGravedad;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(puntoDeGravedad.transform.position, new Vector3(ejeX,ejeY,ejeZ),angulo);
    }
}
