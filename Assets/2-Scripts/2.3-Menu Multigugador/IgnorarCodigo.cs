using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class IgnorarCodigo : MonoBehaviour
{
    [SerializeField] MonoBehaviour[] ignorarScripts;
    void Start()
    {
        if (! GetComponent<PhotonView>().IsMine)
        {
            foreach(MonoBehaviour script in ignorarScripts)
            {
                script.enabled = false;
            }
        }
    }
}
