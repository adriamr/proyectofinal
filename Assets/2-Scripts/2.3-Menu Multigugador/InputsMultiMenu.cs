using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class InputsMultiMenu : MonoBehaviour
{
    //Boton volver y nombre de usuario
    [SerializeField] public Button btVolver;
    [SerializeField] public TMP_Text txNick;
    [SerializeField] public TMP_Text txCargando;


    //Menu Multijugador Principal
    [SerializeField] public GameObject menuMulti;
    [SerializeField] public Button btCrearSala;
    [SerializeField] public Button btBuscarSala;
    [SerializeField] public Button btSalirMultiJugador;

    //Men� Crear Sala
    [SerializeField] public GameObject menuCrearSala;
    [SerializeField] public TMP_InputField inpNombreSala;
    [SerializeField] public Button btCrearLaSala;


    //Men� Buscar Sala
    [SerializeField] public GameObject menuBuscarSala;
    [SerializeField] public RectTransform contenedorSalas;
    [SerializeField] public Button btRefrescar;
    [SerializeField] public Button btSalaPrefab;

    //Men� Lobby
    [SerializeField] public GameObject menuLobby;
    [SerializeField] public TMP_Text txNombreSalaLobby;
    [SerializeField] public TMP_Text txListaJugadores;
    [SerializeField] public Button btJugar;

    //MenuMultijugador multijugador;

    private void Awake()
    {
        //multijugador = GetComponent<MenuMultijugador>();
    }

    private void Start()
    {
        btVolver.onClick.AddListener(() =>
        {
            if (PhotonNetwork.InRoom)
            {
                print("ha salido de room");
                MenuMultijugador.menuMulti.SalirLobbyDePartida();
            }
            ActivarMenuMulti();
        });

        //Men� multi principal
        btCrearSala.onClick.AddListener(() =>
        {
            ActivarMenuCrearSala();
        });

        btBuscarSala.onClick.AddListener(() =>
        {
            ActivarMenuBuscarSala();
            MenuMultijugador.menuMulti.RefrescarListaDeSalas();
        });

        btSalirMultiJugador.onClick.AddListener(() =>
        {
            MenuMultijugador.menuMulti.SalirMenuMultijugador();
        });

        //men� crear sala
        btCrearLaSala.onClick.AddListener(() =>
        {
            NetworkManager.networkManager.CrearSala(inpNombreSala.text);
            btCrearLaSala.interactable = false;
        });

        //men� buscar sala
        btRefrescar.onClick.AddListener(() =>
        {
            MenuMultijugador.menuMulti.RefrescarListaDeSalas();
        });

        //men� lobby
        btJugar.onClick.AddListener(() =>
        {
            NetworkManager.networkManager.photonView.RPC("CargarNivel", RpcTarget.All, "Escenario1");
            //NetworkManager.networkManager.CargarNivel("Escenario1");
            btJugar.interactable = false;
        });
    }

    public void ActivarListener(Button boton, string nombreSala)
    {
        boton.onClick.RemoveAllListeners();
        boton.onClick.AddListener(() =>
        {
            NetworkManager.networkManager.EntrarEnSala(nombreSala);
            //ActivarMenuLobby();
            boton.interactable = false;
        });
    }

    public void ActivarTexto(TMP_Text texto, bool valor)
    {
        texto.gameObject.SetActive(valor);
    }

    public void ActivarMenuMulti()
    {
        //AudioManager.audioManager.ReproducirAudio("AudioBtAtras", 1, new Vector3());
        ActivarBoton(btSalirMultiJugador, true);
        ActivarBoton(btVolver, false);
        ActivarMenu(true, false, false, false);
    }

    private void ActivarMenuCrearSala()
    {
        ActivarBoton(btSalirMultiJugador, false);
        ActivarBoton(btVolver, true);
        ActivarMenu(false, true, false, false);
        btCrearLaSala.interactable = true;
    }

    private void ActivarMenuBuscarSala()
    {
        ActivarBoton(btSalirMultiJugador, false);
        ActivarBoton(btVolver, true);
        ActivarMenu(false, false, true, false);
    }

    public void ActivarMenuLobby()
    {
        btJugar.interactable = true;
        ActivarMenu(false, false, false, true);
    }

    private void ActivarMenu( bool  multi, bool crear, bool buscar, bool lobby)
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
        menuMulti.SetActive(multi);
        menuCrearSala.SetActive(crear);
        menuBuscarSala.SetActive(buscar);
        menuLobby.SetActive(lobby);
    }

    private void ActivarBoton(Button boton, bool valor)
    {
        boton.gameObject.SetActive(valor);
    }

}
