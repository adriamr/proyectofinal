using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager networkManager;
    InputsMultiMenu inputsMenu;
    [SerializeField] byte maximoJugadores = 8;
    List<Button> listaSalas;
    string versionJuego = "0.2";

    void Awake()
    {
        print("awake nm");
        //  multijugador = FindObjectOfType<MenuMultijugador>();
        inputsMenu = FindObjectOfType<InputsMultiMenu>();
        networkManager = this;
        DontDestroyOnLoad(this.gameObject);
        //PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        print("start nm");
        Conectar();
    }

    private void Conectar()
    {
        print("Conectar");
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = versionJuego;
        }
    }

    public void SalirDelNivel()
    {
        PhotonNetwork.LeaveRoom();
        CargarNivel("MenuMultijugador");
        //MenuMultijugador.menuMulti.VisualizarYAbrirMenuMulti(true);

        //PhotonNetwork.Disconnect();
    }

    [PunRPC]
    private void CargarNivel(string nombreNivel)
    {
        MenuMultijugador.menuMulti.VisualizarSala(false);
        PhotonNetwork.LoadLevel(nombreNivel);
    }

    public void CrearSala(string nombreSala)
    {
        PhotonNetwork.CreateRoom(nombreSala, new RoomOptions { MaxPlayers = maximoJugadores });
    }

    public void EntrarEnSala(string nombreSala)
    {
        PhotonNetwork.JoinRoom(nombreSala);
    }

    public void SalirMenuMultijugador()
    {
        //PhotonNetwork.LeaveLobby();
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene("MenuPrincipal");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        MenuMultijugador.menuMulti.infoListaSalas = roomList;
    }

    public override void OnConnectedToMaster()
    {
        print("OnConnectedToMaster nm");
        print("OnConnectedToMaster m");
        inputsMenu.ActivarMenuMulti();
        inputsMenu.ActivarTexto(inputsMenu.txCargando, false);
        inputsMenu.ActivarTexto(inputsMenu.txNick, true);
        //PhotonNetwork.AutomaticallySyncScene = true;

        PhotonNetwork.JoinLobby();

        //PhotonNetwork.JoinLobby();//descomentar si no va


        //inputsMenu.ActivarMenuMulti();
        //inputsMenu.ActivarTexto(inputsMenu.txCargando, false);
        //inputsMenu.ActivarTexto(inputsMenu.txNick, true);
        //PhotonNetwork.AutomaticallySyncScene = true;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        PhotonNetwork.LoadLevel("MenuMultijugador");
        print("OnDisconnected nm");
    }

    public override void OnJoinedRoom()
    {
        print("OnJoinedRoom m");
        inputsMenu.ActivarMenuLobby();
        MenuMultijugador.menuMulti.photonView.RPC("ActualizarLobby", RpcTarget.All);
        //ActualizarLobby();
    }

}