using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Firebase.Auth;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using Unity.VisualScripting;
using ExitGames.Client.Photon;

public class MenuMultijugador : MonoBehaviourPunCallbacks
{
    public static MenuMultijugador menuMulti;
    InputsMultiMenu inputsMenu;
    List<GameObject> listaBotonesSalas;
    public List<RoomInfo> infoListaSalas;


    void Awake()
    {
        menuMulti = this;
        listaBotonesSalas = new List<GameObject>();
        infoListaSalas = new List<RoomInfo>();
        inputsMenu = GetComponent<InputsMultiMenu>();
        PhotonNetwork.NickName = inputsMenu.txNick.text = GameStatus.usuario.Nick;
        //PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        AudioManager.audioManager.ReproducirMusica();
        if (PhotonNetwork.InRoom)
        {
            VisualizarSala(true);
        }
    }

    public void SalirLobbyDePartida()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void VisualizarSala(bool valor)
    {
        PhotonNetwork.CurrentRoom.IsVisible = valor;
        PhotonNetwork.CurrentRoom.IsOpen = valor;
    }

    public void SalirMenuMultijugador()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioBtAtras", 1, new Vector3());
        NetworkManager.networkManager.SalirMenuMultijugador() ;
    }

    private GameObject CrearBotonSala()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
        GameObject boton = Instantiate(inputsMenu.btSalaPrefab.gameObject, inputsMenu.contenedorSalas.transform);
        listaBotonesSalas.Add(boton);
        return boton;
    }

    [PunRPC]
    public void ActualizarLobby()
    {
        print("ActualizarLobby");
        inputsMenu.txListaJugadores.text = "";
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            inputsMenu.txListaJugadores.text+= p.NickName +"\n";
        }
        inputsMenu.txNombreSalaLobby.text = PhotonNetwork.CurrentRoom.Name;
        if (PhotonNetwork.IsMasterClient)
            inputsMenu.btJugar.interactable = true;
        else
            inputsMenu.btJugar.interactable = false;
    }

    public void RefrescarListaDeSalas()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
        foreach (GameObject boton in listaBotonesSalas)
        {
            boton.SetActive(false);
        }

        for (int i = 0; i < infoListaSalas.Count; i++)
        {
            GameObject boton;
            if (i >= listaBotonesSalas.Count)
                boton = CrearBotonSala();
            else
                boton = listaBotonesSalas[i];

            boton.SetActive(true);
            TMP_Text[] textosBoton = boton.GetComponentsInChildren<TMP_Text>();
            textosBoton[0].text = infoListaSalas[i].Name;
            textosBoton[1].text = infoListaSalas[i].PlayerCount.ToString() + "/" + infoListaSalas[i].MaxPlayers;
            Button bt = boton.GetComponent<Button>();
            inputsMenu.ActivarListener(bt, infoListaSalas[i].Name);
        }
    }
}
