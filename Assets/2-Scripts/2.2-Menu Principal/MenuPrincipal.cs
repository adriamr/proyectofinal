using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MenuPrincipal : MonoBehaviour
{
    [SerializeField] TMP_Text txNick;
    private FirebaseAuth _auth;

    private void Awake()
    {

    }
    private void Start()
    {
        AudioManager.audioManager.ReproducirMusica();
        _auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        //db = FindObjectOfType<DBManager>();
        txNick.text = GameStatus.usuario.Nick;
    }
    public void Multijugador()
    {
        SceneManager.LoadScene("MenuMultijugador");
    }

    public void Ranking()
    {
        SceneManager.LoadScene("Ranking");
    }

    public void CerrarSesion()
    {
        _auth.SignOut();
        SceneManager.LoadScene("Inicio");
    }

    public void SalirJuego()
    {
        Application.Quit();
    }
}
