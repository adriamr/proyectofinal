using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputsMenuPrincipal : MonoBehaviour
{
    [SerializeField] Button btMultijugador;
    [SerializeField] Button btRanking;
    [SerializeField] Button btIniciarSesion;
    [SerializeField] Button btSalir;

    MenuPrincipal menu;

    private void Awake()
    {
        menu = GetComponent<MenuPrincipal>();
    }
    void Start()
    {
        
        btMultijugador.onClick.AddListener(() =>
        {
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
            menu.Multijugador();
        });
        btRanking.onClick.AddListener(() =>
        {
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
            menu.Ranking();
        });
        btIniciarSesion.onClick.AddListener(() =>
        {
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3  ());
            menu.CerrarSesion();
        });
        btSalir.onClick.AddListener(() =>
        {
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAtras", 1, new Vector3());
            menu.SalirJuego();
        });
    }
}
