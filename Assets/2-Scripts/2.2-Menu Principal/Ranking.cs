using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Ranking : MonoBehaviour
{
    [SerializeField] public GameObject txCargando;
    [SerializeField] public GameObject datosRanking;
    [SerializeField] public GameObject datosJugador;
    [SerializeField] public RectTransform contenedorPuntuaciones;
    [SerializeField] Button btVolver;
    [SerializeField] Button btMaxPuntos;
    [SerializeField] Button btTotalPuntos;
    [SerializeField] Button btVictorias;

    DBManager db;
    private List<Usuario> listaRanking;

    public List<Usuario> ListaRanking { get => listaRanking; set => listaRanking = value; }

    private void Awake()
    {
        db = FindObjectOfType<DBManager>();
        ListaRanking = new List<Usuario>();
    }
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.audioManager.ReproducirMusica();
        StartCoroutine(db.ObtenerListaJugadores1("MaximaPuntuacion"));
        //StartCoroutine(Rellenar());

        btVolver.onClick.AddListener(() =>
        {
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAtras", 1, new Vector3());
            SceneManager.LoadScene("MenuPrincipal");
        });
        btMaxPuntos.onClick.AddListener(() =>
        {
            limpiarLista();
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
            StartCoroutine(db.ObtenerListaJugadores1("MaximaPuntuacion"));
        });
        btTotalPuntos.onClick.AddListener(() =>
        {
            limpiarLista();
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
            StartCoroutine(db.ObtenerListaJugadores1("PuntosTotales"));
        });
        btVictorias.onClick.AddListener(() =>
        {
            limpiarLista();
            AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
            StartCoroutine(db.ObtenerListaJugadores1("PartidasGanadas"));
        });
    }

    public void limpiarLista()
    {
        //contenedorPuntuaciones.DetachChildren();
        for (byte i=0; i<contenedorPuntuaciones.childCount;i++)
        {
            Destroy(contenedorPuntuaciones.GetChild(i).gameObject);
        }
        ListaRanking.Clear();
    }
    public IEnumerator RellenarInvertir(List<Usuario> ListaRanking)
    {
        yield return new WaitForSecondsRealtime(1f);
        txCargando.gameObject.SetActive(false);
        datosRanking.gameObject.SetActive(true);
        byte i = 9;
        byte c = 1;
        while (i > 0)
        {
            print(i);
            print(ListaRanking[i].Nick);
            GameObject datos = datosJugador;
            TMP_Text[] txDatos = datos.GetComponentsInChildren<TMP_Text>();
            txDatos[0].text = (c).ToString();
            txDatos[1].text = ListaRanking[i].Nick;
            txDatos[2].text = ListaRanking[i].PartidasGanadas.ToString();
            txDatos[3].text = ListaRanking[i].MaximaPuntuacion.ToString();

            Instantiate(datos.gameObject, contenedorPuntuaciones.transform);

            i--;
            c++;
        }
    }

    public void RellenarRanking(Usuario u)
    {
        ListaRanking.Add(new Usuario(u));
        if (ListaRanking.Count==10)
        {
            pintarDatosRanking();
        }
    }

    public void pintarDatosRanking()
    {
        txCargando.gameObject.SetActive(false);
        datosRanking.gameObject.SetActive(true);
        byte c = 1;
        for (int i = 9; i >=0; i--)
        {
            GameObject datos = datosJugador;
            TMP_Text[] txDatos = datos.GetComponentsInChildren<TMP_Text>();
            txDatos[0].text = (c++).ToString();
            txDatos[1].text = ListaRanking[i].Nick;
            txDatos[2].text = ListaRanking[i].PartidasGanadas.ToString();
            txDatos[3].text = ListaRanking[i].PuntosTotales.ToString();
            txDatos[4].text = ListaRanking[i].MaximaPuntuacion.ToString();
            Instantiate(datos.gameObject, contenedorPuntuaciones.transform);
        }
    }

}
