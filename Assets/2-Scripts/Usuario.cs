using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Usuario
{
    private string email;
    private string idUsuario;
    private int maximaPuntuacion;
    private string nick;
    private int nivelActual;
    private int numeroBajas;
    private int numeroMuertes;
    private int partidasGanadas;
    private int partidasJugadas;
    private int puntosTotales;

    public Usuario()
    {
        Email = "";
        IdUsuario = "";
        MaximaPuntuacion = 0;
        Nick = "";
        NivelActual = 0;
        NumeroBajas = 0;
        NumeroMuertes = 0;
        PartidasGanadas = 0;
        PartidasJugadas = 0;
        PuntosTotales = 0;

    }
    public Usuario(Usuario usuario)
    {
        Email = usuario.Email;
        IdUsuario = usuario.IdUsuario;
        MaximaPuntuacion = usuario.MaximaPuntuacion;
        Nick = usuario.Nick;
        NivelActual = usuario.NivelActual;
        NumeroBajas = usuario.NumeroBajas;
        NumeroMuertes = usuario.NumeroMuertes;
        PartidasGanadas = usuario.PartidasGanadas;
        PartidasJugadas = usuario.PartidasJugadas;
        PuntosTotales = usuario.PuntosTotales;
    }

    public Usuario(string nick, string email, string id)
    {
        Email = email;
        IdUsuario = id;
        MaximaPuntuacion = 0;
        Nick = nick;
        NivelActual = 0;
        NumeroBajas = 0;
        NumeroMuertes = 0;
        PartidasGanadas = 0;
        PartidasJugadas = 0;
        PuntosTotales = 0;
    }


    public string Email { get => email; set => email = value; }
    public int MaximaPuntuacion { get => maximaPuntuacion; set => maximaPuntuacion = value; }
    public string Nick { get => nick; set => nick = value; }
    public int NivelActual { get => nivelActual; set => nivelActual = value; }
    public int NumeroBajas { get => numeroBajas; set => numeroBajas = value; }
    public int NumeroMuertes { get => numeroMuertes; set => numeroMuertes = value; }
    public int PartidasGanadas { get => partidasGanadas; set => partidasGanadas = value; }
    public int PartidasJugadas { get => partidasJugadas; set => partidasJugadas = value; }
    public int PuntosTotales { get => puntosTotales; set => puntosTotales = value; }
    public string IdUsuario { get => idUsuario; set => idUsuario = value; }

    public int ComprobarPuntucacionMaxima(int p)
    {
        return MaximaPuntuacion < p ? p : MaximaPuntuacion;
    }
}
