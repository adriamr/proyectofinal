using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eTags
{
    SuperSalto,
    Velocidad,
    Resistencia,
    Vida,
    Jetpack,
    Player,
    Suelo,
    Pistola
}

