using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utiles : MonoBehaviour
{

    public static Vector3 PosicionAleatoria(float minX, float maxX, float minY,float maxY)
    {
        return new Vector3(Rdm(minX, maxX), Rdm( minY, maxY),0);
    }

    public static Vector3 OrientarDireccionJugadorMv(Vector3 escalaActual, float Horizontal, float escala)
    {
        Vector3 escalar = escalaActual;
        if (Horizontal < 0f)
            escalar = Utiles.Invertir(-escala, escala);
        if (Horizontal > 0f)
            escalar=  Utiles.Invertir(escala, escala);
        return escalar;
    }
    public static int Rdm(int desde, int hasta)
    {
        return Random.Range(desde, hasta);
    }
    public static float Rdm(float desde, float hasta)
    {
        return Random.Range(desde, hasta);
    }

    public static Vector3 Invertir(float x, float escala)
    {
        return new Vector3(x, escala, escala);
    }

    private bool EstaInvertido(float Horizontal)
    {
        bool invertido = false;
        if (Horizontal > 0.1f)
            invertido = false;
        else if (Horizontal < -0.1f)
            invertido = true;
        return invertido;
    }

    public static string ObtenerSiguienteID(int id)
    {
        string idUsuario;
        id++;
        if (id.ToString().Length == 1)
            idUsuario = "0000" + id.ToString();
        else if (id.ToString().Length == 2)
            idUsuario = "000" + id.ToString();
        else if (id.ToString().Length == 3)
            idUsuario = "00" + id.ToString();
        else if (id.ToString().Length == 4)
            idUsuario = "0" + id.ToString();
        else
            idUsuario = id.ToString();

        return idUsuario;
    }


    //public static string ObtenerSonidoImpacto()
    //{
    //    string sonido = "";
    //    switch (Random.Range(1,4))
    //    {
    //        case 1:
    //            sonido = "AudioImpactoBala1";
    //            break;
    //        case 2:
    //            sonido = "AudioImpactoBala2";
    //            break;
    //        case 3:
    //            sonido=" AudioImpactoBala3";
    //            break;
    //    }
    //    return sonido;
    //}

}
