using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IniciarSesion : MonoBehaviour
{
    //FirebaseUser usuario;
    DBManager db;
    InputsIniciarSesion inputs;

    private void Awake()
    {
        inputs = GetComponent<InputsIniciarSesion>();
       db = FindObjectOfType<DBManager>();
        
    }
    private void Start()
    {
        AudioManager.audioManager.ReproducirMusica();
    }
    //public FirebaseUser Usuario { get => usuario; set => usuario = value; }
    private void Update()
    {
        if (db.SesionIniciada && GameStatus.usuario != null)
        {
            IrMenuPrincipal();
        }     
    }

    public void InicioSesion(string email, string pass)
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
        db.IniciarSesion(email, pass);
        //db.CargarUsuario(email);
        StartCoroutine(db.CargarUsuario(email, (Usuario usu) => { GameStatus.usuario = usu;}));
    }
    public void Registrarse(string nick, string email, string pass)
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
        db.CrearUsuarioConEmail(email, pass);
        db.GuardarUsuario(nick, email);
    }

    private void IrMenuPrincipal()
    {
        AudioManager.audioManager.ReproducirAudioJuego("AudioBtAvanzar", 1, new Vector3());
        if (GameStatus.usuario != null)
        {
            SceneManager.LoadScene("MenuPrincipal");

        }
    }
}
