using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatus : MonoBehaviour
{
    public static Usuario usuario;

    void Awake()
    {
        if (FindObjectsOfType<GameStatus>().Length >1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }        
    }

}
