using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class InputsIniciarSesion : MonoBehaviour
{
    IniciarSesion inicio;
    //Iniciar sesion
    [SerializeField] Button btLogearse;
    [SerializeField] Button btRegistrarUsuario;
    [SerializeField] TMP_InputField inpPassIS;
    [SerializeField] TMP_InputField inpEmailIS;

    //Registrar usuario
    [SerializeField] Button btRegistrase;
    [SerializeField] Button btIniciarSesion;
    [SerializeField] TMP_InputField inpNick;
    [SerializeField] TMP_InputField inpPassRU;
    [SerializeField] TMP_InputField inpEmailRU;


    [SerializeField] GameObject panelIS;
    [SerializeField] GameObject panelRU;
    [SerializeField] GameObject panelCargando;


    private void Awake()
    {
        inicio = GetComponent<IniciarSesion>() ;
    }

    void Start()
    {
        StartCoroutine(CargaInicio());
        //Panel inicio
        //Boton que se encarga de iniciar sesi�n (Inicio sesi�n)
        btLogearse.onClick.AddListener(() =>
        {
            inicio.InicioSesion(inpEmailIS.text, inpPassIS.text);
            btLogearse.interactable = false;

        });
        //Boton para cambiar de inicio de sesion a registro usuario (Inicio sesi�n)
        btRegistrarUsuario.onClick.AddListener(() =>
        {
            CambiarALogin(false);
        });


        //Panel registro
        // boton para registrar usuario (Registro usuario)
        btRegistrase.onClick.AddListener(() =>
        {
            inicio.Registrarse(inpNick.text, inpEmailRU.text, inpPassRU.text);
            btRegistrase.interactable = false;
        });
        //Boton para cambiar de registro usuario a inicio sesi�n (Registro usuario)
        btIniciarSesion.onClick.AddListener(() =>
        {
            CambiarALogin(true);
        });


    }

    private void CambiarALogin(bool valor)
    {
        panelIS.SetActive(valor);
        btLogearse.interactable = valor;
        panelRU.SetActive(!valor);
        btRegistrase.interactable = !valor;
    }

    IEnumerator CargaInicio()
    {
        yield return new WaitForSecondsRealtime(5f);
        panelCargando.SetActive(false);
        CambiarALogin(true);
    }
}
