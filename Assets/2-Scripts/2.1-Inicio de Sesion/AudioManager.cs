using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager :  MonoBehaviourPunCallbacks
{
    public static AudioManager audioManager;
    [SerializeField] List<GameObject> ListaAudios;

    /// <summary>
    /// Lista de objetos y sus nombres
    /// </summary>
    Dictionary<string, GameObject> dAudios;

    /// <summary>
    /// M�todo que se ejecuta antes de iniciarse el objeto, inicializa audioManager como singleton, inicializa dAudios y le asigna el nombre (del objeto) como clave y el objeto como valor de todos los objetos agregados en listaAudios, posteriormente limpia listaAudios.
    /// </summary>
    private void Awake()
    {
        audioManager = this;
        DontDestroyOnLoad(this.gameObject);
        dAudios = new();
        foreach (GameObject a in ListaAudios)
        {
            dAudios.Add(a.name.ToString(), a);
        }
        ListaAudios.Clear();
    }



    /// <summary>
    /// M�todo que instancia objetos para reproducir audios a partir de los par�metros recibidos
    /// </summary>
    /// <param name="audio">Nombre del GameObject que se va a reproducir</param>
    /// <param name="duracion">Tiempo que dura la reproducci�n antes de destruirse</param>
    /// <param name="posicion">Posici�n donde se reproduce el audio</param>
    public void ReproducirAudioJuego(string audio, float duracion, Vector3 posicion)
    {
        Destroy(Instantiate(dAudios[audio], posicion, Quaternion.identity),duracion);
        //Destroy(PhotonNetwork.Instantiate(audio, posicion, Quaternion.identity), duracion);
    }



    /// <summary>
    /// M�todo que instancia el objeto que reproduce m�sica, con el mismo nombre que el parametro recibido
    /// </summary>
    /// <param name="audio"> string con el nombre del objeto que reproduce el audio</param>
    public void ReproducirMusica(string audio)
    {
        Instantiate(dAudios[audio], new Vector3(), Quaternion.identity);  //Activar cuando se termine de afinar los audios

    }
    /// <summary>
    /// M�todo que instancia un objeto aleatorio que reproduce m�sica
    /// </summary>
    public void ReproducirMusica()
    {
        Instantiate(dAudios["AudioM" + UnityEngine.Random.Range(1, 6).ToString() + "Juego"], new Vector3(), Quaternion.identity);
        //PhotonNetwork.Instantiate("AudioM" + UnityEngine.Random.Range(1, 6).ToString() + "Juego", new Vector3(), Quaternion.identity);
    }
}
