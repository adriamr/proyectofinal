using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuthManager: MonoBehaviour
{
    //RegistrarUsuario registrarUsuario;
    IniciarSesion iniciarSesion;
    protected FirebaseAuth _auth;
    protected FirebaseUser _user;

    string displayName;
    bool signedIn;

    private void Awake()
    {
        //registrarUsuario = null;
        iniciarSesion = null;
    }
    void Start()
    {
        InitializeFirebase();
    }

    // Update is called once per frame
    void Update()
    {
        if (signedIn)
        {

        }
    }

    void InitializeFirebase()
    {
        _auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        _auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (_auth.CurrentUser != _user)
        {
            signedIn = _user != _auth.CurrentUser && _auth.CurrentUser != null;
            if (!signedIn && _user != null)
            {
                Debug.Log("Signed out " + _user.UserId);
            }
            _user = _auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + _user.UserId);
                displayName = _user.DisplayName ?? "";
                //emailAddress = _user.Email ?? "";
                //photoUrl = _user.PhotoUrl ?? "";
            }
        }
    }


    public void IniciarSesion(string email, string pass)
    {


        _auth.SignInWithEmailAndPasswordAsync(email, pass).ContinueWith(task => {
            iniciarSesion = FindObjectOfType<IniciarSesion>();
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }
            FirebaseUser user;
            user = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                user.DisplayName, user.UserId);
            //iniciarSesion.Usuario = user;
        });
    }

    public void ObtenerPerfilUsuario()
    {
        FirebaseUser user = _auth.CurrentUser;
        if (user != null)
        {
            string name = user.DisplayName;
            string email = user.Email;
            //System.Uri photo_url = user.PhotoUrl;
            // The user's Id, unique to the Firebase project.
            // Do NOT use this value to authenticate with your backend server, if you
            // have one; use User.TokenAsync() instead.
            string uid = user.UserId;
        }
    }
}
