using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class DBManager : MonoBehaviour
{
    DatabaseReference referencia;
    FirebaseAuth _auth;
    FirebaseUser _user;
    string idUsuario;
    IniciarSesion iniciar;
    bool sesionIniciada;
    Ranking ranking;
    public bool SesionIniciada { get => sesionIniciada; set => sesionIniciada = value; }

    private void Awake()
    {
        
        iniciar = FindObjectOfType<IniciarSesion>();
        if (FindObjectsOfType<DBManager>().Length > 1)      
            Destroy(gameObject);      
        else  
            DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Se ejecuta al iniciar el objeto, inicializa los valores
    /// </summary>
    void Start()
    {

        FirebaseDatabase db = FirebaseDatabase.GetInstance("https://thelastone-2022-default-rtdb.europe-west1.firebasedatabase.app/");
        referencia = db.RootReference;
        //referencia = FirebaseDatabase.DefaultInstance.RootReference;
        InitializeFirebase();
        ProcesarID();
    }

    public void GuardarUsuario(string nick, string email)
    {
        Usuario usuario = new Usuario(nick, email, idUsuario);

        //string jsUsuario = JsonUtility.ToJson(usuario);
        //referencia.Child("Usuarios").Child(userId).SetRawJsonValueAsync(jsUsuario);

        referencia.Child("Usuarios").Child(idUsuario).Child("Nick").SetValueAsync(usuario.Nick);
        referencia.Child("Usuarios").Child(idUsuario).Child("Email").SetValueAsync(usuario.Email);
        referencia.Child("Usuarios").Child(idUsuario).Child("IdUsuario").SetValueAsync(usuario.IdUsuario);
        referencia.Child("Usuarios").Child(idUsuario).Child("PartidasJugadas").SetValueAsync(usuario.PartidasJugadas);
        referencia.Child("Usuarios").Child(idUsuario).Child("PartidasGanadas").SetValueAsync(usuario.PartidasGanadas);
        referencia.Child("Usuarios").Child(idUsuario).Child("NumeroMuertes").SetValueAsync(usuario.NumeroMuertes);
        referencia.Child("Usuarios").Child(idUsuario).Child("NumeroBajas").SetValueAsync(usuario.NumeroBajas);
        referencia.Child("Usuarios").Child(idUsuario).Child("PuntosTotales").SetValueAsync(usuario.PuntosTotales);
        referencia.Child("Usuarios").Child(idUsuario).Child("MaximaPuntuacion").SetValueAsync(usuario.MaximaPuntuacion);
        referencia.Child("Usuarios").Child(idUsuario).Child("NivelActual").SetValueAsync(usuario.NivelActual);

        referencia.Child("UltimaID").SetValueAsync(idUsuario);
        referencia.Child("Emails").Child(idUsuario).SetValueAsync(email);
        GameStatus.usuario = usuario;

    }

    public IEnumerator CargarUsuario(string email, Action<Usuario> onCallback)
    {
        //iniciar.EstaCargando(true);
        Usuario u = new Usuario() ;
        var usuarios = referencia.Child("Usuarios").GetValueAsync();
        yield return new WaitUntil(predicate: () => usuarios.IsCompleted);

        if (usuarios!=null)
        {
            DataSnapshot snapshot = usuarios.Result;
            foreach (var usu in snapshot.Children)
            {
                u.Email = usu.Child("Email").Value.ToString();
                u.IdUsuario = usu.Child("IdUsuario").Value.ToString();
                u.Nick = usu.Child("Nick").Value.ToString();
                u.NivelActual = int.Parse(usu.Child("NivelActual").Value.ToString());
                u.PartidasJugadas = int.Parse(usu.Child("PartidasJugadas").Value.ToString());
                u.PartidasGanadas = int.Parse(usu.Child("PartidasGanadas").Value.ToString());
                u.NumeroMuertes = int.Parse(usu.Child("NumeroMuertes").Value.ToString());
                u.NumeroBajas = int.Parse(usu.Child("NumeroBajas").Value.ToString());
                u.PuntosTotales = int.Parse(usu.Child("PuntosTotales").Value.ToString());
                u.MaximaPuntuacion = int.Parse(usu.Child("MaximaPuntuacion").Value.ToString());


                // u = JsonUtility.FromJson<Usuario>(usu.GetRawJsonValue());
                if (u.Email == email)
                {
                    GameStatus.usuario = u;
                    break;

                }
                onCallback.Invoke(u);     
            }
            
        }
        //iniciar.EstaCargando(false);
    }
    public IEnumerator ObtenerListaJugadores1(string ordenarPor)
    {
        ranking = FindObjectOfType<Ranking>();
        Usuario u = new Usuario();
        var usuarios = referencia.Child("Usuarios").OrderByChild(ordenarPor).LimitToLast(10).GetValueAsync();
        yield return new WaitUntil(predicate: () => usuarios.IsCompleted);

        if (usuarios != null)
        {
            DataSnapshot snapshot = usuarios.Result;
            foreach (var usu in snapshot.Children)
            {
                u.Email = usu.Child("Email").Value.ToString();
                u.IdUsuario = usu.Child("IdUsuario").Value.ToString();
                u.Nick = usu.Child("Nick").Value.ToString();
                u.NivelActual = int.Parse(usu.Child("NivelActual").Value.ToString());
                u.PartidasJugadas = int.Parse(usu.Child("PartidasJugadas").Value.ToString());
                u.PartidasGanadas = int.Parse(usu.Child("PartidasGanadas").Value.ToString());
                u.NumeroMuertes = int.Parse(usu.Child("NumeroMuertes").Value.ToString());
                u.NumeroBajas = int.Parse(usu.Child("NumeroBajas").Value.ToString());
                u.PuntosTotales = int.Parse(usu.Child("PuntosTotales").Value.ToString());
                u.MaximaPuntuacion = int.Parse(usu.Child("MaximaPuntuacion").Value.ToString());

                ranking.RellenarRanking(u);
            }
        }
    }


    void InitializeFirebase()
    {
        _auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        _auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (_auth.CurrentUser != _user)
        {
            SesionIniciada = _user != _auth.CurrentUser && _auth.CurrentUser != null;
            if (!SesionIniciada && _user != null)
            {
                Debug.Log("Signed out " + _user.UserId);
            }
            _user = _auth.CurrentUser;
            if (SesionIniciada)
            {
                Debug.Log("Usuario Registrado: " + _user.Email);
                StartCoroutine(CargarUsuario(_user.Email, (Usuario usu) => { GameStatus.usuario = usu; }));
            }
        }
    }

    public void CrearUsuarioConEmail(string email, string password)
    {
        _auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            FirebaseUser newUser = task.Result;
            newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }

    public void ActualizarPuntuacionesUsuario(Usuario usu)
    {
        referencia.Child("Usuarios").Child(usu.IdUsuario).Child("MaximaPuntuacion").SetValueAsync(usu.MaximaPuntuacion);
        referencia.Child("Usuarios").Child(usu.IdUsuario).Child("NivelActual").SetValueAsync(usu.NivelActual);
        referencia.Child("Usuarios").Child(usu.IdUsuario).Child("NumeroBajas").SetValueAsync(usu.NumeroBajas);
        referencia.Child("Usuarios").Child(usu.IdUsuario).Child("NumeroMuertes").SetValueAsync(usu.NumeroMuertes);
        referencia.Child("Usuarios").Child(usu.IdUsuario).Child("PartidasJugadas").SetValueAsync(usu.PartidasJugadas);
        referencia.Child("Usuarios").Child(usu.IdUsuario).Child("PartidasGanadas").SetValueAsync(usu.PartidasGanadas);
        referencia.Child("Usuarios").Child(usu.IdUsuario).Child("PuntosTotales").SetValueAsync(usu.PuntosTotales);
    }

    public void IniciarSesion(string email, string pass)
    {
        _auth.SignInWithEmailAndPasswordAsync(email, pass).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }
            _user = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                _user.DisplayName, _user.UserId);
            StartCoroutine(CargarUsuario(email, (Usuario usu) => { GameStatus.usuario = usu; }));
        });
    }


    public void ProcesarID()
    {
        referencia.Child("UltimaID").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                print("task is faulted");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                int id = int.Parse(snapshot.Value.ToString());               
                idUsuario = Utiles.ObtenerSiguienteID(id);
            }
        });
    }
}
